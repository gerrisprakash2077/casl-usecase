import React from 'react'
import { useState } from 'react'
export default function Create(props) {
    const [values, setValues] = useState({
        title: '',
        content: '',
        description: ''
    })
    return (
        <div>
            <div className='fixed top-1/2  bg-slate-300 p-10'>
                <div className='p-2 flex justify-between' >
                    <label htmlFor="title">Title:</label>
                    <input value={values.title}
                        onChange={(event) => {
                            setValues({
                                ...values,
                                title: event.target.value
                            })
                        }}
                        type="text" />
                </div>
                <div className='p-2 flex justify-between'>
                    <label htmlFor="content">content:</label>
                    <input value={values.content}
                        onChange={(event) => {
                            setValues({
                                ...values,
                                content: event.target.value
                            })
                        }}
                        type="text" />
                </div>
                <div className='p-2 flex justify-between'>
                    <label htmlFor="description">description:</label>
                    <input
                        onChange={(event) => {
                            setValues({
                                ...values,
                                description: event.target.value
                            })
                        }}
                        value={values.description} type="text" />
                </div>
                <div className='flex justify-between mt-4'>
                    <button className='bg-white px-2 py-1 '
                        onClick={() => {
                            props.addPosts(values.title, values.content, values.description)
                            props.setCreateSet()
                            setValues({
                                title: '',
                                content: '',
                                description: ''
                            })
                        }}
                    >save</button>
                    <button className='bg-white px-2 py-1 ' onClick={() => {
                        props.setCreateSet()
                        setValues({
                            title: '',
                            content: '',
                            description: ''
                        })
                    }}>cancel</button>
                </div>
            </div>
        </div>
    )
}
