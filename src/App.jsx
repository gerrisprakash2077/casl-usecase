import { useState } from "react"
import { defineAbility } from './casl/caslconfig'
import { Can } from './Can'
import { AbilityContext } from './Can'
import Create from './components/Create'
export default function App() {
  const [role, setRole] = useState('Select role')
  const [posts, setPost] = useState([])
  const [create, setCreate] = useState(false)
  function setCreateSet() {
    if (!create) {
      setCreate(true)
    } else {
      setCreate(false)
    }
  }
  function addPosts(title, content, decription) {
    if (posts.length == 0) {
      setPost([
        ...posts,
        {
          title: title,
          content: content,
          discription: decription,
          id: 0
        }
      ])
    } else {
      setPost([
        ...posts,
        {
          title: title,
          content: content,
          discription: decription,
          id: posts[posts.length - 1].id + 1
        }
      ])
    }
  }
  return (
    <>
      <div className="text-center bg-slate-300 py-4">
        <h2 className="inline pr-3 text-xl">select Role:</h2>
        <select onChange={(event) => {
          setRole(event.target.value)
        }} name="role" id="role">
          <option value="guest">GUEST</option>
          <option value="user">USER</option>
        </select>
      </div>
      <h2 className="text-2xl text-center p-5">{role}</h2>
      <div className='w-max ml-auto mr-auto'>
        {posts.map((post) => {
          return <div key={post.id} className='my-10'>
            <h2 className="text-xl my-5">{post.title}</h2>
            <p className="my-5">{post.content}</p>
            <p className="text-sm my-5">{post.discription}</p>
            <div className="flex gap-10">
              {defineAbility(role).can('delete', 'Posts') &&
                <button onClick={() => {
                  let filteredPost = posts.filter((elem) => {
                    return elem.id != post.id
                  })
                  setPost([...filteredPost])
                }} className="bg-blue-300 py-1 px-3">delete</button>}
            </div>
          </div>
        })}
        {defineAbility(role).can('create', 'Posts') &&
          <button onClick={setCreateSet} className="bg-blue-300 py-1 px-3">create</button>}
      </div>
      {create ? <Create setCreateSet={setCreateSet} addPosts={addPosts} /> : null}
    </>

  )
}
