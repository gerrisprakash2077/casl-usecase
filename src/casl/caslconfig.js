import { Ability, AbilityBuilder } from '@casl/ability';
export function defineAbility(user) {
    const { can, cannot, build } = new AbilityBuilder(Ability);
    if (user == 'user') {
        can('manage', 'all')
    }
    return build()
}
const ability = defineAbility('admin')

console.log(ability.can('create', 'Posts'));