import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App'
import './index.css'
import { AbilityContext } from './Can'
import { defineAbility } from './casl/caslconfig'
ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <AbilityContext.Provider value={defineAbility}>
      <App />
    </AbilityContext.Provider>
  </React.StrictMode>,
)
